#include <parallelus/ParallelUS.hpp>
#include "org_parallelus_app_pusnetworker_PUSNetworker.hpp"

#include <jni.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <android/log.h>
#include <stddef.h>
#include <android/log.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <tuple>
#include <android/bitmap.h>
#include <string>

using namespace parallelus;

struct NativeData {
	std::shared_ptr<Runtime> runtime;
	std::shared_ptr<Program> program;
	std::shared_ptr<NetworkManager> network;
};

void clearAll(KernelArgument &argsBuffers, std::list<std::string> &taskKernelsNames){
	taskKernelsNames.clear();
	argsBuffers.clear();
}

void printTextView(JNIEnv *env, jobject jtextViewObject, std::string message){

	jclass clazz = (*env).FindClass("android/widget/TextView");
    jmethodID setText = (*env).GetMethodID(clazz, "setText", "(Ljava/lang/CharSequence;)V");

    jstring jstr = (*env).NewStringUTF(message.c_str());
    (*env).CallVoidMethod(jtextViewObject, setText, jstr);
}

JNIEXPORT jlong JNICALL Java_org_parallelus_app_pusnetworker_PUSNetworker_nativeInit
	(JNIEnv *env, jobject self) {

	JavaVM *jvm;
	env->GetJavaVM(&jvm);
	if(!jvm) return (jlong) nullptr;

	auto dataPointer = new NativeData();
	dataPointer->runtime = std::make_shared<Runtime>(jvm,false);
	dataPointer->network = std::make_shared<NetworkManager>();
	return (jlong) dataPointer;
}

JNIEXPORT int JNICALL Java_org_parallelus_app_pusnetworker_PUSNetworker_nativeFind
	(JNIEnv *env, jobject self, jlong dataLong, jobject jtextViewObject) {

	auto dataPointer = (NativeData *) dataLong;
	int ok = dataPointer->network->createSocket();

	if(!ok){		
		return 0;
	}

	printTextView(env,jtextViewObject,"ParallelUS Grayscale");
	return 1;
}

JNIEXPORT int JNICALL Java_org_parallelus_app_pusnetworker_PUSNetworker_nativeConect
	(JNIEnv *env, jobject self, jlong dataLong) {

	auto dataPointer = (NativeData *) dataLong;
	int ok = dataPointer->network->conect(5775,"150.164.200.114");

	if(!ok) return 0;
	return 1;
}

JNIEXPORT void JNICALL Java_org_parallelus_app_pusnetworker_PUSNetworker_nativeCleanUp
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	delete dataPointer;
}

JNIEXPORT void JNICALL Java_org_parallelus_app_pusnetworker_PUSNetworker_nativeNetworker
        (JNIEnv *env, jobject self, jlong dataLong, jobject jtextViewObject) {

	auto dataPointer = (NativeData *) dataLong;

	KernelArgument argsBuffers;
	std::list<std::string> taskKernelsNames;

	std::string msgTag;
	bool done = false;

	while(!done){
		__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[WAITING]...");
		//printTextView(env,jtextViewObject,"0. Waiting Task from Server...");
		msgTag = dataPointer->network->receiveTagMessage();

		if(!strcmp(msgTag.c_str(),"KERNEL____")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[KERNEL]Beginning");
			std::string gKernels;
			gKernels = dataPointer->network->receiveKernelString();
			dataPointer->program = std::make_shared<Program>(dataPointer->runtime,gKernels.c_str());
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[KERNEL]Done");
		}
		else if(!strcmp(msgTag.c_str(),"KERNELNAME")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[KERNELNAME]Beginning");
			std::string kernelName;
			kernelName = dataPointer->network->receiveKernelString();
			taskKernelsNames.push_back(kernelName);
			std::string msg;
			msg = "1. Received Task: " + kernelName;
			//printTextView(env,jtextViewObject,msg.c_str());
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[KERNELNAME]Done");
		}
		else if(!strcmp(msgTag.c_str(),"ARG_______")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[ARG]Beginning");
			dataPointer->network->receiveArgBuffer(argsBuffers);
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[ARG]Done");
		}
		else if(!strcmp(msgTag.c_str(),"WORKSIZE__")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[WORKSIZE]Beginning");
			dataPointer->network->receiveWorkSize(argsBuffers);
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[WORKSIZE]Done");
		}
		else if(!strcmp(msgTag.c_str(),"EXECUTE___")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[EXECUTE]Beginning");
			//printTextView(env,jtextViewObject,"2. Executing Received Task.");
			auto task = std::make_unique<Task>(dataPointer->program);
			for(std::list<std::string>::iterator it = taskKernelsNames.begin(); it != taskKernelsNames.end(); it++){
				task->addKernel((*it));
			}

			task->setConfigFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type){
				for(kernelArgument_it it = argsBuffers.begin();	it != argsBuffers.end(); it++){
					int id = 0;
					for(bufferList_it it2 = it->second.first.begin(); it2 != it->second.first.end(); it2++){
						kernelHash[it->first]->setArg(id,(*it2),type);
						id++;
					}
					kernelHash[it->first]->setWorkSize(std::get<0>(it->second.second),
						std::get<1>(it->second.second),
						std::get<2>(it->second.second),type);
				}

			});
			dataPointer->runtime->submitTask(std::move(task),2);
			dataPointer->runtime->finish();
			dataPointer->network->sendString("DONE______");
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[EXECUTE]Done");
			//printTextView(env,jtextViewObject,"3. Task Executed.");
		}
		else if(!strcmp(msgTag.c_str(),"OUTPUT____")){
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[OUTPUT]Beginning");
			//printTextView(env,jtextViewObject,"4. Sending Results.");
			std::string kernelName;
			kernelName = dataPointer->network->receiveKernelString();
			int bufferId = dataPointer->network->receiveInt();
			int id = 0;
			for(bufferList_it it2 = argsBuffers[kernelName].first.begin();
				it2 != argsBuffers[kernelName].first.end(); it2++){
				if(id == bufferId){
					dataPointer->network->sendOutputBuffer((*it2));
				}
				else id++;
			}
			//done = true;
			__android_log_print(ANDROID_LOG_ERROR, "DEBUG","[OUTPUT]Done");
			//printTextView(env,jtextViewObject,"5. Finished.");
			clearAll(argsBuffers,taskKernelsNames);
		}
	}
}
