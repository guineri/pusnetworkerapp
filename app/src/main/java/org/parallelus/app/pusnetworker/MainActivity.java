package org.parallelus.app.pusnetworker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.parallelus.app.pusnetworker.pusnetworker.R;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    Button searchButton;
    Button conectButton;
    TextView appName;

    PUSNetworker networker;

    public int findPUSServer(View view, TextView appName) {
        return networker.findPUSapplication(appName);
    }

    public int conectPUSServer(View view) {
        return networker.conectPUSapplication();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        networker = new PUSNetworker();

        searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int f = findPUSServer(v,appName);
                if(f==1){
                    appName.setVisibility(View.VISIBLE);
                    conectButton.setVisibility(View.VISIBLE);
                }
            }
        });

        appName = (TextView) findViewById(R.id.appName);
        appName.setVisibility(View.INVISIBLE);
        conectButton = (Button) findViewById(R.id.conect_button);
        conectButton.setVisibility(View.INVISIBLE);
        conectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int c = conectPUSServer(v);
                if (c==1) {
                    Context context = getApplicationContext();
                    CharSequence text = "Conection ok!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    AppInteractionActivity.networker = networker;
                    Intent intent = new Intent(MainActivity.this, AppInteractionActivity.class);
                    startActivity(intent);
                }
            }
        });

    }
}

