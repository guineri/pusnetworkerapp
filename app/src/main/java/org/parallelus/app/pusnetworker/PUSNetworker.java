package org.parallelus.app.pusnetworker;

import android.widget.TextView;

/**
 * Runs network interations with ndk codes
 * @author Guilherme Andrade
 */
public class PUSNetworker {
    private long dataPointer = nativeInit();

    private native long nativeInit();
    private native int nativeFind(long dataPointer, TextView appName);
    private native int nativeConect(long dataPointer);
    private native void nativeCleanUp(long dataPointer);
    private native void nativeNetworker(long dataPointer, TextView log);

    @Override
    protected void finalize() throws Throwable {
        nativeCleanUp(dataPointer);
        super.finalize();
    }

    public void startNetworker(TextView log) {
        nativeNetworker(dataPointer,log);
    }

    public int findPUSapplication(TextView appName) {
        return nativeFind(dataPointer,appName);
    }

    public int conectPUSapplication() {
        return nativeConect(dataPointer);
    }

    static {
        System.loadLibrary("Networker");
    }
}
