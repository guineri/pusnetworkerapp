package org.parallelus.app.pusnetworker;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.parallelus.app.pusnetworker.pusnetworker.R;
import org.w3c.dom.Text;

public class AppInteractionActivity extends AppCompatActivity {

    TextView log;
    public static PUSNetworker networker;
    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interaction);

        TextView appName = (TextView) findViewById(R.id.appName);
        appName.setText("ParallelUS Grayscale");
        TextView ipport = (TextView) findViewById(R.id.ipport);
        ipport.setText("192.168.10.102:5775");

        log = (TextView) findViewById(R.id.log);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                networker.startNetworker(log);
            }
        });
        thread.start();

    }
}
